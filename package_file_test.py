import requests
import os

# %% constants
# https://gitlab.com/tulkas-hg/read-package-files-test/-/packages/13463086
TEST_PACKAGE_ID = 13463086
# %% functions


def upload_package_file(
    package_name: str, version: str, filename: str, data: bytes
) -> requests.Response:
    """Upload file to the package repository of a GitLab project.

    Required authentication:
        - Deploy Token (write_package_registry)
        - Personal Access Token (api)
        - Project Access Token (api)

    See also: https://docs.gitlab.com/ee/user/packages/generic_packages/#publish-a-package-file
    """
    headers = {"JOB-TOKEN": os.environ["CI_JOB_TOKEN"]}
    r = requests.put(
        (
            f"{os.environ['CI_API_V4_URL']}"
            f"/projects/{os.environ['CI_PROJECT_ID']}/packages/generic/"
            f"{package_name}/{version}/{filename}"
        ),
        data=data,
        headers=headers,
    )
    r.raise_for_status()
    if not r.status_code == 201:
        raise requests.HTTPError(r.status_code)
    return r


def get_package_file_list(package_id: int) -> list:
    """Return a list of all package files of a given package version."""
    headers = {"JOB-TOKEN": os.environ["CI_JOB_TOKEN"]}
    request_url = (
        f"{os.environ['CI_API_V4_URL']}"
        f"/projects/{os.environ['CI_PROJECT_ID']}/packages/{package_id}/package_files"
    )
    return _get_all_pages(request=request_url, headers=headers)


def _get_all_pages(
    request: str,
    headers: dict,
    per_page: int = 100,
    params: dict = {},
) -> list:
    """Get all pages of GitLab data request"""
    my_params = params.copy()
    my_params["per_page"] = per_page
    result_list = []
    while 1:
        r = requests.get(request, headers=headers, params=my_params)
        r.raise_for_status()
        new_listitems = r.json()
        # check for other pages if the item was not found
        pages = _Gl_pagination_parser(r)
        result_list += new_listitems
        # print('page:', pages.x_page, 'of', pages.x_total_pages)
        if pages.next is None:
            break
        # The url for the next page is derived from the answer.
        # The original params have to be cleared.
        request = pages.next
        my_params.clear()
    return result_list


class _Gl_pagination_parser:
    """Parse the GitLAB REST API response to navigate through pages"""

    def __init__(self, response: requests.Response) -> None:
        headers = response.headers
        self.next = None
        self.prev = None

        self.link_entry = str(headers["link"])
        self.x_page = int(headers["x-page"])
        self.x_per_page = int(headers["x-per-page"])
        self.x_total = int(headers["x-total"])
        self.x_total_pages = int(headers["x-total-pages"])

        # Get the links to navigate through the pages
        dLinks = response.links
        if "next" in dLinks:
            self.next = dLinks["next"]["url"]
            self.x_next_page = int(headers["x-next-page"])
        if "prev" in dLinks:
            self.prev = dLinks["prev"]["url"]
            self.x_next_page = int(headers["x-prev-page"])
        if "first" in dLinks:
            self.first = dLinks["first"]["url"]
        if "last" in dLinks:
            self.last = dLinks["last"]["url"]


def test_upload():
    """My test for GitLab.com"""
    version = "0.0.1"
    text = f"This is my file {os.environ['CI_PIPELINE_IID']}"
    filename = f"file_{os.environ['CI_PIPELINE_IID']}.txt"
    package_name = "my_generic_package"
    upload_package_file(
        package_name=package_name,
        version=version,
        filename=filename,
        data=text.encode(encoding="utf-8"),
    )


def list_package_files():
    file_list = get_package_file_list(package_id=TEST_PACKAGE_ID)
    for file in file_list:
        print(file["file_name"])


if __name__ == "__main__":
    test_upload()
    list_package_files()
# %%
